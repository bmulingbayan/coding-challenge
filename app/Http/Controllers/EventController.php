<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventRequest;
use App\Models\Event;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class EventController extends Controller
{
    public function index()
    {
        return Inertia::render('Index', [
            'events' => Event::all()
        ]);
    }

    public function store(EventRequest $request)
    {
        $startDate = Carbon::parse($request->start_date);
        $endDate = Carbon::parse($request->end_date);

        Event::where('start', '>=', $startDate)
            ->where('end', '<=', $endDate)
            ->delete();

        $data = collect(CarbonPeriod::create($startDate, $endDate)->toArray())
            ->filter(function ($date) use ($request) {
                return in_array($date->dayOfWeek, $request->days);
            })
            ->map(function ($date) use ($request) {
                return [
                    'start' => $date->format('Y-m-d'),
                    'end' => $date->format('Y-m-d'),
                    'title' => $request->title
                ];
            })
            ->values()
            ->toArray();

        Event::insert($data);

        return redirect('/');
    }
}
