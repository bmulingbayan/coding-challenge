<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_create_event()
    {
        $response = $this->post('/events', [
            'title' => $this->faker()->paragraph,
            'start_date' => Carbon::now()->subMonth()->format('Y-m-d'),
            'end_date' => Carbon::now()->addDay()->format('Y-m-d'),
            'days' => $this->faker()->randomElements([
                Carbon::SUNDAY,
                Carbon::MONDAY,
                Carbon::TUESDAY,
                Carbon::WEDNESDAY,
                Carbon::THURSDAY,
                Carbon::FRIDAY,
                Carbon::SATURDAY
            ], $this->faker()->numberBetween(1, 7)),
        ]);

        $response->assertStatus(302);
    }

    public function test_can_not_create_event_with_invalid_data()
    {
        $response = $this->post('/events', []);

        $response->assertStatus(302)
            ->assertSessionHasErrors(['title', 'start_date', 'end_date', 'days']);
    }
}
